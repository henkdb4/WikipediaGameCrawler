const std = @import("std");
const curl = @import("curl.zig");
const parse = @import("parse.zig");
const String = @import("string").String;

const wikiUrl = "https://en.wikipedia.org";
// const wikiUrl = "http://localhost:8080";

const start = "/wiki/Canada_goose";
const eind = "/wiki/Lua_(programming_language)";

pub fn main() !void {
    var arena_state = std.heap.ArenaAllocator.init(std.heap.c_allocator);
    defer arena_state.deinit();

    const allocator = arena_state.allocator();

    var pages = std.ArrayList([]const u8).init(allocator);
    defer pages.deinit();

    try pages.append(start);

    var i: usize = 0;

    while (i < pages.items.len) : (i += 1) {
        const page = pages.items[i];

        std.log.info("Fetching Page: {s}", .{page});

        const url = try parse.concatString(allocator, wikiUrl, page);
        defer allocator.free(url);

        const stuff = try curl.getContent(allocator, url);
        defer allocator.free(stuff);

        var linksOnPage = std.ArrayList([]const u8).init(allocator);
        defer linksOnPage.deinit();

        try parse.findHtmlLinks(stuff, &linksOnPage);

        for (linksOnPage.items) |link| {
            if (link.len < 6 or !std.mem.eql(u8, link[0..6], "/wiki/")) {
                continue;
            }

            const found = try allocator.alloc(u8, link.len);
            std.mem.copyForwards(u8, found, link);
            try pages.append(found);

            if (std.mem.eql(u8, found, eind)) {
                return;
            }
        }
    }
}
