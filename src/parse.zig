const std = @import("std");

pub fn findHtmlLinks(content: []const u8, buffer: *std.ArrayList([]const u8)) !void {
    for (0.., content) |i, c| {
        if (c == 'h'
        // and i + 6 < content.len
        ) { // Might be a link
            if (std.mem.eql(u8, content[i .. i + 4], "href")) { // Is a link, lets find the start
                var start = i;
                var end = i;

                for (start.., content[start..]) |j, d| { // search from h to find "
                    if (d == '"') {
                        start = j + 1;
                        break;
                    }
                }

                for (start.., content[start..]) |j, d| { // search from start to find second "
                    if (d == '"') {
                        end = j;
                        break;
                    }
                }

                try buffer.append(content[start..end]);

                //                std.log.info("Found link from pos {d} to pos {d}: {s}", .{ start, end, content[start..end] });
            }
        }
    }
}

pub fn concatString(allocator: std.mem.Allocator, str1: []const u8, str2: []const u8) ![]const u8 {
    var buffer = try allocator.alloc(u8, str1.len + str2.len);
    errdefer allocator.free(buffer);

    std.mem.copyForwards(u8, buffer[0..], str1);
    std.mem.copyForwards(u8, buffer[str1.len..], str2);

    return buffer;
}
