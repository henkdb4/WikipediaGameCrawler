const std = @import("std");
const cURL = @cImport({
    @cInclude("curl/curl.h");
});

const curlError = error{
    CURLHandleInitFailed,
    CURLGlobalInitFailed,
    CouldNotSetURL,
    CouldNotSetWriteCallback,
    FailedToPerformRequest,
    CurlErrorError,
    OutOfMemory,
    ConfigurationFailed,
};

pub fn getContent(allocator: std.mem.Allocator, url: []const u8) curlError![]u8 {
    if (cURL.curl_global_init(cURL.CURL_GLOBAL_ALL) != cURL.CURLE_OK)
        return error.CURLGlobalInitFailed;
    defer cURL.curl_global_cleanup();

    std.log.info("Url: {s}", .{url});
    const handle = cURL.curl_easy_init() orelse return error.CURLHandleInitFailed;
    defer cURL.curl_easy_cleanup(handle);

    var response_buffer = std.ArrayList(u8).init(allocator);
    errdefer response_buffer.deinit();

    var err_buffer = std.ArrayList(u8).init(allocator);
    defer err_buffer.deinit();

    const c_url = try allocator.dupeZ(u8, url);
    defer allocator.free(c_url);

    if (cURL.curl_easy_setopt(handle, cURL.CURLOPT_URL, c_url.ptr) != cURL.CURLE_OK)
        return error.CouldNotSetURL;

    if (cURL.curl_easy_setopt(handle, cURL.CURLOPT_WRITEFUNCTION, writeToArrayListCallback) != cURL.CURLE_OK)
        return error.CouldNotSetWriteCallback;
    if (cURL.curl_easy_setopt(handle, cURL.CURLOPT_WRITEDATA, &response_buffer) != cURL.CURLE_OK)
        return error.CouldNotSetWriteCallback;

    if (cURL.curl_easy_perform(handle) != cURL.CURLE_OK) {
        var errcode: f64 = 123;
        if (cURL.curl_easy_getinfo(handle, cURL.CURLINFO_HTTP_CONNECTCODE, &errcode) != cURL.CURLE_OK) {
            std.debug.print("CURL couldn't even produce an error\n", .{});
            return error.CurlErrorError;
        }
        std.debug.print("CURL exited with error: {d}\n", .{errcode});
        std.debug.print("\tMsg: {s}\n", .{err_buffer.items});
        std.debug.print("\tBody: {s}\n", .{response_buffer.items});

        return error.FailedToPerformRequest;
    }

    return try response_buffer.toOwnedSlice();
}

fn writeToArrayListCallback(data: *anyopaque, size: c_uint, nmemb: c_uint, user_data: *anyopaque) callconv(.C) c_uint {
    var buffer: *std.ArrayList(u8) = @alignCast(@ptrCast(user_data));
    var typed_data: [*]u8 = @ptrCast(data);
    buffer.appendSlice(typed_data[0 .. nmemb * size]) catch return 0;
    return nmemb * size;
}
